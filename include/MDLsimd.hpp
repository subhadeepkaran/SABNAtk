/***
 *  $Id$
 **
 *  File: MDLsimd.hpp
 *  Created: Apr 04, 2019
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2019 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MDL_SIMD_HPP
#define MDL_SIMD_HPP

#include <cmath>
#include <utility>


class MDL {
public:
    typedef std::pair<double, double> score_type;

    explicit MDL(int m = 0) : m_(m) { }

    void init(int ri, int qi) { score_ = 0.0; nc_ = 0.0; ri_ = ri; qi_ = qi; count_ = 0; }

    void finalize(int qi) {
        // Uncomment to change how number of observed states in handled
        // qi_ = qi;
        for (int i = 0; i < count_; ++i) score_ += (nijk_[i] * log2(p_[i]));
        nc_ = 0.5 * log2(m_) * (ri_ - 1) * qi_;
    } // finalize

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) {
        p_[count_] = static_cast<double>(Nijk) / Nij;
        nijk_[count_] = Nijk;

        count_++;

        if (count_ == SIMD_SIZE) {
            for (int i = 0; i < SIMD_SIZE; ++i) score_ += (nijk_[i] * log2(p_[i]));
            count_ = 0;
        }
    } // operator()

    int r() const { return ri_; }

    score_type score() const { return {-(score_ - nc_), -score_}; }


private:
    static const int SIMD_SIZE = 32;

    alignas(16) double p_[SIMD_SIZE];
    alignas(16) double nijk_[SIMD_SIZE];

    int count_ = 0;

    double nc_ = 0.0;
    double score_ = 0.0;

    int m_  = 0;
    int ri_ = 1;
    int qi_ = 1;

}; // class MDL

#endif // MDL_SIMD_HPP
