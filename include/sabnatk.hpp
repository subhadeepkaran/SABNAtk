/***
 *  $Id$
 **
 *  File: sabnatk.hpp
 *  Created: May 20, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016-2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 */

#ifndef SABNATK_HPP
#define SABNATK_HPP

#include "BVCounter.hpp"
#include "RadCounter.hpp"

#include "AIC.hpp"
#include "BDeu.hpp"
#include "MDL.hpp"


template <int N, typename Iter>
inline void create_query_engine(int n, int m, Iter iter, BVCounter<N>& bvc) {
    bvc = create_BVCounter<N>(n, m, iter);
} // create_query_engine

template <int N, typename Iter>
inline void create_query_engine(int n, int m, Iter iter, RadCounter<N>& bvc) {
    bvc = create_RadCounter<N>(n, m, iter);
} // create_query_engine

#endif // SABNATK_HPP
