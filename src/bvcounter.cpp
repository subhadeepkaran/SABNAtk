/***
 *  $Id$
 **
 *  File: bvcounter.cpp
 *  Created: Nov 06, 2017
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <cstring>
#include <vector>

#include <AIC.hpp>
#include <BDeu.hpp>
#include <BVCounter.hpp>
#include <MDL.hpp>
#include <bit_util.hpp>

#include <boost/python.hpp>


namespace bpy = boost::python;


namespace bvc {

  template <int N> class bvcounter_base {
  public:
      using data_type = uint8_t;
      using set_type = typename BVCounter<N>::set_type;

      void init(int n, int m, const bpy::list& L) {
          p_bvc_ = create_BVCounter<N>(n, m, bpy::stl_input_iterator<data_type>(L));
      } // init

      void reorder(const bpy::list& L) {
          auto n = bpy::len(L);

          std::vector<int> v(n);
          for (int i = 0; i < n; ++i) v[i] = bpy::extract<int>(L[i]);

          p_bvc_.reorder(v);
      } // reorder

      int r(int xi) const { return p_bvc_.r(xi); }

  protected:
      BVCounter<N> p_bvc_;

  }; // class bvcounter_base


  template <int N> class AICtk : public bvcounter_base<N> {
  public:
      using data_type = typename bvcounter_base<N>::data_type;
      using set_type = typename bvcounter_base<N>::set_type;

      bpy::list score(const bpy::str& xi, const bpy::str& pa) {
          const char* xi_str = bpy::extract<const char*>(xi);
          int xi_len = bpy::extract<int>(pa.attr("__len__")());

          set_type xi_set = set_empty<set_type>();
          std::memcpy(&xi_set, xi_str, xi_len);

          const char* pa_str = bpy::extract<const char*>(pa);
          int pa_len = bpy::extract<int>(pa.attr("__len__")());

          set_type pa_set = set_empty<set_type>();
          std::memcpy(&pa_set, pa_str, pa_len);

          int n_xi = set_size(xi_set);
          bpy::list res;

          F_.resize(n_xi, AIC());
          this->p_bvc_.apply(xi_set, pa_set, F_);

          for (int i = 0; i < n_xi; ++i) {
              auto s = F_[i].score();
              res.append(bpy::make_tuple(std::get<0>(s), std::get<1>(s)));
          }

          return res;
      } // score

  private:
      std::vector<AIC> F_;

  }; // class AICtk

  template <int N> class BDeutk : public bvcounter_base<N> {
  public:
      using data_type = typename bvcounter_base<N>::data_type;
      using set_type = typename bvcounter_base<N>::set_type;

      double alpha = 1.0;

      bpy::list score(const bpy::str& xi, const bpy::str& pa) {
          const char* xi_str = bpy::extract<const char*>(xi);
          int xi_len = bpy::extract<int>(pa.attr("__len__")());

          set_type xi_set = set_empty<set_type>();
          std::memcpy(&xi_set, xi_str, xi_len);

          const char* pa_str = bpy::extract<const char*>(pa);
          int pa_len = bpy::extract<int>(pa.attr("__len__")());

          set_type pa_set = set_empty<set_type>();
          std::memcpy(&pa_set, pa_str, pa_len);

          int n_xi = set_size(xi_set);
          bpy::list res;

          F_.resize(n_xi, BDeu(alpha));
          this->p_bvc_.apply(xi_set, pa_set, F_);

          for (int i = 0; i < n_xi; ++i) {
              auto s = F_[i].score();
              res.append(bpy::make_tuple(std::get<0>(s), std::get<1>(s), std::get<2>(s)));
          }

          return res;
      } // score

  private:
      std::vector<BDeu> F_;

  }; // class BDeutk

  template <int N> class MDLtk : public bvcounter_base<N> {
  public:
      using data_type = typename bvcounter_base<N>::data_type;
      using set_type = typename bvcounter_base<N>::set_type;

      bpy::list score(const bpy::str& xi, const bpy::str& pa) {
          const char* xi_str = bpy::extract<const char*>(xi);
          int xi_len = bpy::extract<int>(pa.attr("__len__")());

          set_type xi_set = set_empty<set_type>();
          std::memcpy(&xi_set, xi_str, xi_len);

          const char* pa_str = bpy::extract<const char*>(pa);
          int pa_len = bpy::extract<int>(pa.attr("__len__")());

          set_type pa_set = set_empty<set_type>();
          std::memcpy(&pa_set, pa_str, pa_len);

          int n_xi = set_size(xi_set);
          bpy::list res;

          F_.resize(n_xi, MDL(this->p_bvc_.m()));
          this->p_bvc_.apply(xi_set, pa_set, F_);

          for (int i = 0; i < n_xi; ++i) {
              auto s = F_[i].score();
              res.append(bpy::make_tuple(std::get<0>(s), std::get<1>(s)));
          }

          return res;
      } // score

  private:
      std::vector<MDL> F_;

  }; // class MDLtk

  template <int N> class Query : public bvcounter_base<N> {
  public:
      using data_type = uint8_t;
      using set_type = typename bvcounter_base<N>::set_type;

      bpy::list run(const bpy::list& xi, const bpy::list& pa,
                    const bpy::list& xi_st, const bpy::list& pa_st) {
          set_type xi_set = set_empty<set_type>();
          auto n_xi = bpy::len(xi);
          for (int i = 0; i < n_xi; ++i) xi_set = set_add(xi_set, bpy::extract<int>(xi[i]));

          set_type pa_set = set_empty<set_type>();
          auto n_pa = bpy::len(pa);
          for (int i = 0; i < n_pa; ++i) pa_set = set_add(pa_set, bpy::extract<int>(pa[i]));

          state_xi_.resize(n_xi);
          state_pa_.resize(n_pa);

          for (int i = 0; i < n_xi; ++i) state_xi_[i] = bpy::extract<int>(xi_st[i]);
          for (int i = 0; i < n_pa; ++i) state_pa_[i] = bpy::extract<int>(pa_st[i]);

          F_.resize(n_xi);

          this->p_bvc_.apply(xi_set, pa_set, state_xi_, state_pa_, F_);

          bpy::list res;
          for (int i = 0; i < n_xi; ++i) res.append(F_[i].score());

          return res;
      } // run

  private:
      struct call__ {
          void init(int, int) { nijk = 0; }
          void finalize(int) { }
          void operator()(int) { }
          void operator()(int Nijk, int) { nijk = Nijk; }
          int score() const { return nijk; }
          int nijk = 0;
      }; // struct call__

      std::vector<data_type> state_xi_;
      std::vector<data_type> state_pa_;
      std::vector<call__> F_;

  }; // class Query

} // namespace bvc


void export_BVCounter() {
    bpy::object rc_module(bpy::handle<>(bpy::borrowed(PyImport_AddModule("sabnatk.BVCounter"))));
    bpy::scope().attr("BVCounter") = rc_module;

    bpy::scope rc_scope = rc_module;

    bpy::class_<bvc::AICtk<1>>("AIC64")
        .def("init", &bvc::bvcounter_base<1>::init)
        .def("reorder", &bvc::bvcounter_base<1>::reorder)
        .def("score", &bvc::AICtk<1>::score)
        .def("r", &bvc::bvcounter_base<1>::r);

    bpy::class_<bvc::AICtk<2>>("AIC128")
        .def("init", &bvc::bvcounter_base<2>::init)
        .def("reorder", &bvc::bvcounter_base<2>::reorder)
        .def("score", &bvc::AICtk<2>::score)
        .def("r", &bvc::bvcounter_base<2>::r);

    bpy::class_<bvc::AICtk<4>>("AIC256")
        .def("init", &bvc::bvcounter_base<4>::init)
        .def("reorder", &bvc::bvcounter_base<4>::reorder)
        .def("score", &bvc::AICtk<4>::score)
        .def("r", &bvc::bvcounter_base<4>::r);

    bpy::class_<bvc::BDeutk<1>>("BDeu64")
        .def_readwrite("alpha", &bvc::BDeutk<1>::alpha)
        .def("init", &bvc::bvcounter_base<1>::init)
        .def("reorder", &bvc::bvcounter_base<1>::reorder)
        .def("score", &bvc::BDeutk<1>::score)
        .def("r", &bvc::bvcounter_base<1>::r);

    bpy::class_<bvc::BDeutk<2>>("BDeu128")
        .def_readwrite("alpha", &bvc::BDeutk<2>::alpha)
        .def("init", &bvc::bvcounter_base<2>::init)
        .def("reorder", &bvc::bvcounter_base<2>::reorder)
        .def("score", &bvc::BDeutk<2>::score)
        .def("r", &bvc::bvcounter_base<2>::r);

    bpy::class_<bvc::BDeutk<4>>("BDeu256")
        .def_readwrite("alpha", &bvc::BDeutk<4>::alpha)
        .def("init", &bvc::bvcounter_base<4>::init)
        .def("reorder", &bvc::bvcounter_base<4>::reorder)
        .def("score", &bvc::BDeutk<4>::score)
        .def("r", &bvc::bvcounter_base<4>::r);

    bpy::class_<bvc::MDLtk<1>>("MDL64")
        .def("init", &bvc::bvcounter_base<1>::init)
        .def("reorder", &bvc::bvcounter_base<1>::reorder)
        .def("score", &bvc::MDLtk<1>::score)
        .def("r", &bvc::bvcounter_base<1>::r);

    bpy::class_<bvc::MDLtk<2>>("MDL128")
        .def("init", &bvc::bvcounter_base<2>::init)
        .def("reorder", &bvc::bvcounter_base<2>::reorder)
        .def("score", &bvc::MDLtk<2>::score)
        .def("r", &bvc::bvcounter_base<2>::r);

    bpy::class_<bvc::MDLtk<4>>("MDL256")
        .def("init", &bvc::bvcounter_base<4>::init)
        .def("reorder", &bvc::bvcounter_base<4>::reorder)
        .def("score", &bvc::MDLtk<4>::score)
        .def("r", &bvc::bvcounter_base<4>::r);

    bpy::class_<bvc::Query<1>>("Query64")
        .def("init", &bvc::bvcounter_base<1>::init)
        .def("reorder", &bvc::bvcounter_base<1>::reorder)
        .def("run", &bvc::Query<1>::run);

    bpy::class_<bvc::Query<2>>("Query128")
        .def("init", &bvc::bvcounter_base<2>::init)
        .def("reorder", &bvc::bvcounter_base<2>::reorder)
        .def("run", &bvc::Query<2>::run);

    bpy::class_<bvc::Query<4>>("Query256")
        .def("init", &bvc::bvcounter_base<4>::init)
        .def("reorder", &bvc::bvcounter_base<4>::reorder)
        .def("run", &bvc::Query<4>::run);

} // export_BVCounter
